# Client-server request rate limiter demonstration 

Projects consist of a command line interface (CLI) client application and a simple web (REST) service.


## Client application

Client application consists of multiple request generators and corresponding web clients that propagate transformed requests to a service. Each request generator creates a sequence of requests with randomly varying pauses between consecutive requests.

Client application user is interactively prompted to provide a number of request generators (default is one). The application generates and mediates request to the service until the user terminates the client application by pressing Enter.    

Client application contains a local service implementation as a requests consumer, but it is disabled (used only for testing request generators).


## Web service

The web service is a dummy REST service with a POST method (to avoid potential caching) that merely logs received requests.

The main part of this demonstration code represents the service protection. The service is protected with the request rate limiter that is tested by a service filter on each incoming request. If the request rate limit is exceeded, the service filter aborts the request with HTTP status code 503 (Service Unavailable), otherwise passes the request to the service.  

### Request rate limiter 

The limiter limits request rate per client, i.e. client, client ID respectively, is the scope of the limiter.

Request rate limiter is based on time frames. A time frame begins on the first client's request that is not a part of another time frame. The time frame has a span of 5 seconds. After the time frame expires, a new time frame is created on the next client's request. Time frames in the scope of a single client cannot overlap. Time frames of different users are independent of each other. Within a time frame the limiter permits only up to 5 requests, including the request that started the time frame. Only those permitted request in a time frame per client are regarded by a request filter as acceptable. Not permitted requests within the time frame per client are denied by the request filter (in this demonstration the filter responds with status 503 service unavailable).     


## Getting Started

For building client and server code, Maven is required. 

JDK 11 is required.


## Building and Installing

### Maven

Build client code with fat-JAR package and server code with WAR package in the root project:

    $ ./mvn clean package

> Created artifacts with built code in subprojects within their subdirectories `./target`.


## Running the application

### Service

On a localhost, deploy the WAR file to a web server that supports Java EE 8 (tested with Wildfly 18). Request listening port has to be 8080.

### Client

On Windows run 

	$ java -jar client\target\client-standalone.jar

On Linux run

	$ java -jar ./client/target/client-standalone.jar


## Authors
* Borut Turšič
