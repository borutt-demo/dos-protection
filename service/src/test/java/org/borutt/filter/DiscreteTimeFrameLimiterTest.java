package org.borutt.filter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DiscreteTimeFrameLimiterTest {

    private int timeFrameRequestsCapacity;
    private long timeFrameTimeSpanMilli;
    private Instant atFirstRequest;
    // private Instant inFirstTimeFrame;
    private Instant afterFirstTimeFrame;
    private String clientId;
    private String otherClientId;

    @Spy
    private DiscreteTimeFrameLimiter<String> limiter;

    @BeforeAll
    protected static void setUpBeforeClass() throws Exception {
    }

    @BeforeEach
    protected void setUp() throws Exception {
        timeFrameRequestsCapacity = DiscreteTimeFrameLimiter.MAX_REQUESTS_PER_TIME_FRAME;
        timeFrameTimeSpanMilli = DiscreteTimeFrameLimiter.TIME_FRAME_SPAN_MS;
        atFirstRequest = Instant.parse("2020-01-01T12:00:00.00Z");
        // inFirstTimeFrame = atFirstRequest.plusMillis(timeFrameTimeSpanMilli / 2);
        afterFirstTimeFrame = atFirstRequest.plusMillis(timeFrameTimeSpanMilli + 1);
        clientId = "client-id";
        otherClientId = "other-client-id";
    }

    @Test
    public void givenAbsentTimeFrame_thenPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();
        // WHEN
        boolean passeesRequest1 = limiter.passes(clientId);
        // THEN
        assertTrue(passeesRequest1, "Passes the first request");
    }

    @Test
    public void givenNonExpiredAndNonExhaustedTimeFrame_thenPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();

        // WHEN
        List<Boolean> passesRequests = IntStream.rangeClosed(1, timeFrameRequestsCapacity)
                .mapToObj(i -> limiter.passes(clientId)).collect(Collectors.toList());
        // THEN
        List<Boolean> expectedAllPasses = Stream.generate(() -> Boolean.TRUE).limit(timeFrameRequestsCapacity)
                .collect(Collectors.toList());
        assertIterableEquals(expectedAllPasses, passesRequests,
                "Passes all requests in time frame until it is exhausted");
    }

    @Test
    public void givenNonExpiredAndExhaustedTimeFrame_thenNotPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();
        IntStream.rangeClosed(1, timeFrameRequestsCapacity).forEach(i -> limiter.passes(clientId));

        // WHEN
        boolean passesRequestInExhaustedTimeFrame = limiter.passes(clientId);

        // THEN
        assertFalse(passesRequestInExhaustedTimeFrame, "Refuses a request in exhausted time frime");
    }

    @Test
    public void givenExpiredAndNonExhaustedTimeFrame_thenPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();
        limiter.passes(clientId);
        doReturn(afterFirstTimeFrame).when(limiter).getCurrentTimeInstant();

        // WHEN
        boolean passesRequestAfterExpiredTimeFrame = limiter.passes(clientId);

        // THEN
        assertTrue(passesRequestAfterExpiredTimeFrame, "Passes a request after expired non-exhausted time frame");
    }

    @Test
    public void givenExpiredAndExhaustedTimeFrame_thenPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();
        IntStream.rangeClosed(1, timeFrameRequestsCapacity).forEach(i -> limiter.passes(clientId));
        doReturn(afterFirstTimeFrame).when(limiter).getCurrentTimeInstant();

        // WHEN
        boolean passesRequestAfterExpiredTimeFrame = limiter.passes(clientId);

        // THEN
        assertTrue(passesRequestAfterExpiredTimeFrame, "Passes a request after expired exhausted time frame");
    }

    @Test
    public void givenNonExpiredAndExhaustedTimeFrame_whenOtherClientIssuesRequest_thenPass() {
        // GIVEN
        doReturn(atFirstRequest).when(limiter).getCurrentTimeInstant();
        IntStream.rangeClosed(1, timeFrameRequestsCapacity).forEach(i -> limiter.passes(clientId));

        // WHEN
        boolean passesOtherClientRequest = limiter.passes(otherClientId);

        // THEN
        assertTrue(passesOtherClientRequest,
                "When this client's time frame is exhausted, it passes other client's request");
    }

}
