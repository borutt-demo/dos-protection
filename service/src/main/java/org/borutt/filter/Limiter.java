package org.borutt.filter;

/**
 * Limiter limits requests per time unit in each scope identified by the
 * corresponding unique scope object.
 * <p>
 * Example: Limiting web requests per time unit for each client identified by a
 * unique client ID, where limiter scope is modeled as client ID.
 * </p>
 *
 * @param <T>
 *            Unique limiter scope.
 */
public interface Limiter<T> {

    /**
     * Answers whether the request with the provided ID should be currently
     * permitted according the limiter policy.
     * 
     * @param scope
     *            Non-null key of the scope of the limiting policy.
     * @return <code>true</code> if within borders of limiting policy,
     *         <code>false</code> otherwise.
     */
    boolean passes(T scope);

}