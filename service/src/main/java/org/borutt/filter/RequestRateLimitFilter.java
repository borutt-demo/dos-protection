package org.borutt.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

@Provider
public class RequestRateLimitFilter implements ContainerRequestFilter {

    private static final String QUERY_PARAM_CLIENT_ID = "clientid";
    private static final String ANONYMOUS_CLIENT_ID = "JohnDoe";

    @Inject
    private DiscreteTimeFrameLimiter<String> rateLimiter;

    @Override
    public void filter(ContainerRequestContext reqCtx) throws IOException {
        String clientId = reqCtx.getUriInfo().getQueryParameters().getFirst(QUERY_PARAM_CLIENT_ID);
        if (clientId == null || clientId.isEmpty()) {
            clientId = ANONYMOUS_CLIENT_ID;
        }
        if (!rateLimiter.passes(clientId)) {
            reqCtx.abortWith(Response.status(Status.SERVICE_UNAVAILABLE).entity("Rate limit exceeded.").build());
        }
    }

}
