package org.borutt.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS application context of the service application.
 */
@ApplicationPath("/")
public class ServiceApplication extends Application {
}
