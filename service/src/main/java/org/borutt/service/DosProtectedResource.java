package org.borutt.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple greeting service protected by time-frame request limiter.
 */
@Path("/greeter")
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.TEXT_PLAIN)
public class DosProtectedResource {

    private static final Logger logger = LoggerFactory.getLogger(DosProtectedResource.class);

    @POST
    public String greetClient(@QueryParam("clientid") String clientId) {
        if (clientId == null || clientId.isEmpty()) {
            logger.info("Received request from a stranger");
            return "Hello stranger";
        } else {
            logger.info("Received request from client {}", clientId);
            return "Hello client " + clientId;
        }
    }
}
