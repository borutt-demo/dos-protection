package org.borutt.client.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;

import org.borutt.client.RequestConsumer;
import org.borutt.client.generator.Request;
import org.borutt.client.generator.Response;

/**
 * This is a web client controller. It receives generated requests, generates
 * and issues corresponding HTTP requests. In return it converts received HTTP
 * responses and returns them to the client of this controller.
 */
public class HttpClientController implements RequestConsumer {

    private static final String QUERY_PARAM_CLIENT_ID = "clientid";
    private final HttpClient client;
    private final URI baseResourceUri;

    public HttpClientController(HttpClient client, URI baseResourceUri) {
        this.client = client;
        this.baseResourceUri = baseResourceUri;
    }

    @Override
    public CompletableFuture<Response> sendAsync(Request request) {
        return client.sendAsync(buildHttpRequest(request), BodyHandlers.ofString())
                .thenApply(webResponse -> new Response(Integer.toString(webResponse.statusCode()), request));
    }

    private HttpRequest buildHttpRequest(Request request) {
        try {
            String query = QUERY_PARAM_CLIENT_ID + "=" + request.getClientId();
            URI resourceUri = setQuery(baseResourceUri, query);
            return HttpRequest.newBuilder().uri(resourceUri).POST(BodyPublishers.noBody()).build();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid web request URI syntax.", e);
        }
    }

    private URI setQuery(URI uri, String query) throws URISyntaxException {
        return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), query, uri.getFragment());
    }

}
