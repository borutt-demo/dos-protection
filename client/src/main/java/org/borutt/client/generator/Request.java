package org.borutt.client.generator;

public class Request {

    private final int sequenceNumber;
    private final int clientId;

    public Request(int sequenceNumber, int clientId) {
        this.sequenceNumber = sequenceNumber;
        this.clientId = clientId;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public int getClientId() {
        return clientId;
    }

}
