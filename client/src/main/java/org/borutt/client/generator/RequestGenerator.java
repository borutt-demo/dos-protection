package org.borutt.client.generator;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.borutt.client.RequestConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestGenerator {

    private static final Logger logger = LoggerFactory.getLogger(RequestGenerator.class);

    private static final AtomicInteger requestCounter = new AtomicInteger(0);
    private static final AtomicInteger pendingTasks = new AtomicInteger(0);
    private final List<CompletableFuture<?>> pendingFutures = Collections.synchronizedList(new LinkedList<>());
    private final int id;
    private final int minDelayMs;
    private final int maxDelayMs;
    private final RequestConsumer consumer;
    private final ExecutorService executor;

    public RequestGenerator(int id, RequestConsumer consumer, ExecutorService executor, int minDelayMs,
            int maxDelayMs) {
        this.id = id;
        this.consumer = consumer;
        this.executor = executor;
        this.minDelayMs = minDelayMs;
        this.maxDelayMs = maxDelayMs;
    }

    public static List<RequestGenerator> createGenerators(int nrGenerators, RequestConsumer consumer,
            ExecutorService executor, int minDelayMs, int maxDelayMs) {
        return IntStream.iterate(1, clientId -> clientId + 1).limit(nrGenerators)
                .mapToObj(clientId -> new RequestGenerator(clientId, consumer, executor, minDelayMs, maxDelayMs))
                .collect(Collectors.toList());
    }

    public void start(RequestGeneratorContext context) {
        if (context.continuesExecution) {
            // logger.info("GENERATOR {} STARTING", id);
            CompletableFuture.supplyAsync(this::sendAndReceive, nextRandomDelayedExecutor())
                    .thenAccept(this::storePendingFuture).thenRun(() -> this.start(context));
        }
    }

    private void storePendingFuture(CompletableFuture<?> future) {
        pendingFutures.add(future);
        pendingFutures.removeIf(CompletableFuture::isDone);
    }

    public void stopGarcefully() {
        CompletableFuture.allOf(pendingFutures.toArray(new CompletableFuture[pendingFutures.size()])).join();
        logger.info("GENERATOR {} COMPLETE", id);
    }

    private CompletableFuture<Void> sendAndReceive() {
        pendingTasks.incrementAndGet();
        return CompletableFuture.completedFuture(createRequest()).thenApplyAsync(this::send, executor).thenRun(() -> {
            int nrTasks = pendingTasks.decrementAndGet();
            logger.info("Pending tasks count: {}", nrTasks);
        });
    }

    private CompletableFuture<Response> send(Request request) {
        logger.info("Sending request {} (generator {})", request.getSequenceNumber(), id);
        return consumer.sendAsync(request).thenApply(response -> {
            logger.info("Received response {} (generator {}, status {})",
                    response.getOriginalRequest().getSequenceNumber(), id, response.getStatus());
            return response;
        });
    }

    private Request createRequest() {
        return new Request(requestCounter.incrementAndGet(), id);
    }

    private Executor nextRandomDelayedExecutor() {
        return CompletableFuture.delayedExecutor(getNextRequestDelayinMs(), TimeUnit.MILLISECONDS, executor);
    }

    private int getNextRequestDelayinMs() {
        return ThreadLocalRandom.current().nextInt(minDelayMs, maxDelayMs + 1);
    }

    public static class RequestGeneratorContext {

        private volatile boolean continuesExecution = true;

        public boolean isContinuesExecution() {
            return continuesExecution;
        }

        public void setContinuesExecution(boolean continuesExecution) {
            this.continuesExecution = continuesExecution;
        }

    }

}
