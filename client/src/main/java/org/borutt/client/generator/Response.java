package org.borutt.client.generator;

public class Response {

    public static final String STATUS_CODE_OK = "200";
    public static final String STATUS_CODE_SERVICE_UNAVAILABLE = "503";

    private final String status;
    private final Request originalRequest;

    public Response(Request originalRequest) {
        status = STATUS_CODE_OK;
        this.originalRequest = originalRequest;
    }

    public Response(String status, Request originalRequest) {
        this.status = status;
        this.originalRequest = originalRequest;
    }

    public String getStatus() {
        return status;
    }

    public Request getOriginalRequest() {
        return originalRequest;
    }

}
