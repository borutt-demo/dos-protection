package org.borutt.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.borutt.client.generator.RequestGenerator;
import org.borutt.client.generator.RequestGenerator.RequestGeneratorContext;
import org.borutt.client.localservice.DiscreteTimeFrameLimiter;
import org.borutt.client.localservice.RequestsConsumerLocal;
import org.borutt.client.web.HttpClientController;

/**
 * This is a command line console application for generating requests that are
 * sent to a service.
 */
public class Main {

    // Temporarily hardcoded configuration - should be read from an external
    // resource
    private static final int MIN_REQUEST_DELAY_MS = 0;
    private static final int MAX_REQUEST_DELAY_MS = 300;
    private static final int DEFAULT_NR_CLIENTS = 1;
    private static final boolean USE_WEB_SERVICE = true;
    private static final URI BASE_SERVICE_URI = URI.create("http://localhost:8080/service/greeter/");

    private static final String MSG_PROMPT_ENTER_NR_CLIENTS = MessageFormat
            .format("Enter a number of simulated clients [{0}]: ", DEFAULT_NR_CLIENTS);
    private static final String MSG_OUT_NR_CLIENT = "Number of clients: ";
    private static final String MSG_PROMPT_ENTER_KEY_TO_FINISH = "Press Enter to finish...";
    private static final String MSG_OUT_FINISHING = "Finishing gracefully...";

    private static final ExecutorService executor = Executors
            .newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static void main(String[] args) throws InterruptedException, IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int nrClients = enterNumberOfClients(reader);
            System.out.println(MSG_OUT_NR_CLIENT + nrClients);
            RequestConsumer consumer = buildRequestConsumer();
            RequestGeneratorContext context = new RequestGeneratorContext();
            List<RequestGenerator> clients = RequestGenerator.createGenerators(nrClients, consumer, executor,
                    MIN_REQUEST_DELAY_MS, MAX_REQUEST_DELAY_MS);
            clients.stream().forEach(client -> client.start(context));
            waitUntilKeyPressed(reader);
            context.setContinuesExecution(false);
            clients.stream().forEach(RequestGenerator::stopGarcefully);
            stopExecutor();
        }
    }

    private static int enterNumberOfClients(BufferedReader reader) throws IOException {
        System.out.print(MSG_PROMPT_ENTER_NR_CLIENTS);
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.isBlank()) {
                return DEFAULT_NR_CLIENTS;
            } else {
                try {
                    return Integer.parseInt(line);
                } catch (NumberFormatException e) {
                    System.out.print(MSG_PROMPT_ENTER_NR_CLIENTS);
                }
            }
        }
        return DEFAULT_NR_CLIENTS;
    }

    /**
     * Blocks current thread until a new line (Enter pressed) is read by the
     * provided reader.
     */
    private static void waitUntilKeyPressed(BufferedReader reader) throws IOException {
        System.out.println(MSG_PROMPT_ENTER_KEY_TO_FINISH);
        reader.readLine();
        System.out.println(MSG_OUT_FINISHING);
    }

    public static void stopExecutor() throws InterruptedException {
        executor.shutdown();
        executor.awaitTermination(2000, TimeUnit.MILLISECONDS);
    }

    private static RequestConsumer buildRequestConsumer() {
        if (USE_WEB_SERVICE) {
            HttpClientController controller = buildHttpClientController(executor, BASE_SERVICE_URI);
            return controller::sendAsync;
        } else {
            return new RequestsConsumerLocal(new DiscreteTimeFrameLimiter<>());
        }
    }

    private static HttpClientController buildHttpClientController(Executor executor, URI baseServiceUri) {
        HttpClient client = HttpClient.newBuilder().executor(executor).version(Version.HTTP_1_1).build();
        return new HttpClientController(client, baseServiceUri);
    }

}
