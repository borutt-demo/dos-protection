package org.borutt.client.localservice;

import java.time.Instant;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class DiscreteTimeFrameLimiter<T> implements Limiter<T> {

    private static final int MAX_REQUESTS_PER_TIME_FRAME = 5;
    private static final int TIME_FRAME_SPAN_MS = 5000;
    private final ConcurrentMap<T, TimeFrame> scopes = new ConcurrentHashMap<>();

    @Override
    public boolean passes(T scope) {
        Objects.requireNonNull(scope);
        TimeFrame frame = getCurrentTimeFrame(scope);
        return !frame.countDownAndTestCountDepleted();
    }

    // Thread safe
    private TimeFrame getCurrentTimeFrame(T scope) {
        Instant now = getCurrentTimeInstant();
        return scopes.compute(scope,
                (key, oldFrame) -> (oldFrame == null || oldFrame.isExpired(now)) ? createTimeFrame(now) : oldFrame);
    }

    private TimeFrame createTimeFrame(Instant startTime) {
        return new TimeFrame(new AtomicInteger(MAX_REQUESTS_PER_TIME_FRAME),
                startTime.plusMillis(TIME_FRAME_SPAN_MS).toEpochMilli());
    }

    protected Instant getCurrentTimeInstant() {
        return Instant.now();
    }

    private static class TimeFrame {
        private final AtomicInteger countDownCounter;
        private final Long endTimeMilli;

        public TimeFrame(AtomicInteger countDownCounter, Long endTimeMilli) {
            this.countDownCounter = countDownCounter;
            this.endTimeMilli = endTimeMilli;
        }

        public boolean countDownAndTestCountDepleted() {
            // return countDownCounter.updateAndGet(value -> (value > 0) ? value - 1 :
            // value) <= 0;
            return countDownCounter.getAndDecrement() <= 0; // Possible but unlike integer overflow
        }

        public boolean isExpired(Instant currentTime) {
            return currentTime.toEpochMilli() > endTimeMilli;
        }
    }

}
