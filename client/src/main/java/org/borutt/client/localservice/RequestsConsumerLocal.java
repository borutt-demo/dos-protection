package org.borutt.client.localservice;

import java.util.concurrent.CompletableFuture;

import org.borutt.client.RequestConsumer;
import org.borutt.client.generator.Request;
import org.borutt.client.generator.Response;

/**
 * This is a simple local requests consumer implementation for testing purposes.
 */
public class RequestsConsumerLocal implements RequestConsumer {

    private final Limiter<Integer> limiter;

    public RequestsConsumerLocal(Limiter<Integer> limiter) {
        this.limiter = limiter;
    }

    @Override
    public CompletableFuture<Response> sendAsync(Request request) {
        simulateLatencyDelay();
        boolean passes = limiter.passes(request.getClientId());
        return CompletableFuture.completedFuture(
                new Response(passes ? Response.STATUS_CODE_OK : Response.STATUS_CODE_SERVICE_UNAVAILABLE, request));
    }

    /**
     * Simulates network communication latency delay. It would be more authentic to
     * make latency simulation using a proxy between generator and consumer.
     */
    private void simulateLatencyDelay() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
