package org.borutt.client;

import java.util.concurrent.CompletableFuture;

import org.borutt.client.generator.Request;
import org.borutt.client.generator.Response;

/**
 * Request consumer takes the provided request and asynchronously returns a
 * corresponding response. This interface is used by a request generator.
 *
 */
public interface RequestConsumer {

    CompletableFuture<Response> sendAsync(Request request);

}